﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace Calculator2
{
    public partial class CardGame : System.Web.UI.Page
    {
        public game currentgame;
        public DataTable scorecard = new DataTable();
        public Table showtrick;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["scores"]!=null)
            {
                scorecard = (DataTable)Session["scores"]; 
            }
            if (Session["showntrick"] != null)
            {
                showtrick = (Table)Session["showntrick"];
                if (PlaceHolder_Tricks.HasControls() == false)
                {
                    PlaceHolder_Tricks.Controls.Add(showtrick);
                }
            }
            if (!Page.IsPostBack)
            {
                
            }
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            Session["savedgame"] = currentgame;
            Session["scores"] = scorecard;
            Session["showntrick"] = showtrick;
        }
        public class oneplayer
        {
            public oneplayer(int ID)
            {
                this.playerID = ID;
                this.score = 0;
                this.playerhand = new hand();
            }

            public void dealplayer(int k, deck d)
            {
                for(int i=1; i<=k; i++)
                {
                    card dealtcard = d.dealcard();
                    dealtcard.cardholder = this.playerID;
                    playerhand.Add(dealtcard);
                    this.numwon = 0;
                }
            }
            public int playerID;
            public int score;
            public hand playerhand;
            public int numwanted {get; set;}
            public int numwon { get; set; }
        }

        public class round
        {
            public round(game g, int k)
            {
                rounddeck = new deck();
                biddingstarted = false;
                roundtrump = rounddeck.dealcard();
                foreach (oneplayer p in g.gameplayers)
                {
                    p.playerhand.Clear();
                    p.dealplayer(k, rounddeck);
                }
                g.gameplayers[0].playerhand.organize();
            }
            public List<trick> tricks;
            public card roundtrump;
            public deck rounddeck;
            public bool biddingstarted;
        }

        public class game : List<round>
        {
            public game(players p) : base()
            {
                gameplayers = p;
                numplayers = p.Count();
            }
            public players gameplayers;
            public int numplayers;
        }

        public class deck: List<card>
        {
            public deck() : base() {
                this.addcard("H", 2, "images/51.png");
                this.addcard("H", 3, "images/47.png");
                this.addcard("H", 4, "images/43.png");
                this.addcard("H", 5, "images/39.png");
                this.addcard("H", 6, "images/35.png");
                this.addcard("H", 7, "images/31.png");
                this.addcard("H", 8, "images/27.png");
                this.addcard("H", 9, "images/23.png");
                this.addcard("H", 10, "images/19.png");
                this.addcard("H", 11, "images/15.png");
                this.addcard("H", 12, "images/11.png");
                this.addcard("H", 13, "images/7.png");
                this.addcard("H", 14, "images/3.png");
                this.addcard("D", 2, "images/52.png");
                this.addcard("D", 3, "images/48.png");
                this.addcard("D", 4, "images/44.png");
                this.addcard("D", 5, "images/40.png");
                this.addcard("D", 6, "images/36.png");
                this.addcard("D", 7, "images/32.png");
                this.addcard("D", 8, "images/28.png");
                this.addcard("D", 9, "images/24.png");
                this.addcard("D", 10, "images/20.png");
                this.addcard("D", 11, "images/16.png");
                this.addcard("D", 12, "images/12.png");
                this.addcard("D", 13, "images/8.png");
                this.addcard("D", 14, "images/4.png");
                this.addcard("S", 2, "images/50.png");
                this.addcard("S", 3, "images/46.png");
                this.addcard("S", 4, "images/42.png");
                this.addcard("S", 5, "images/38.png");
                this.addcard("S", 6, "images/34.png");
                this.addcard("S", 7, "images/30.png");
                this.addcard("S", 8, "images/26.png");
                this.addcard("S", 9, "images/22.png");
                this.addcard("S", 10, "images/18.png");
                this.addcard("S", 11, "images/14.png");
                this.addcard("S", 12, "images/10.png");
                this.addcard("S", 13, "images/6.png");
                this.addcard("S", 14, "images/2.png");
                this.addcard("C", 2, "images/49.png");
                this.addcard("C", 3, "images/45.png");
                this.addcard("C", 4, "images/41.png");
                this.addcard("C", 5, "images/37.png");
                this.addcard("C", 6, "images/33.png");
                this.addcard("C", 7, "images/29.png");
                this.addcard("C", 8, "images/25.png");
                this.addcard("C", 9, "images/21.png");
                this.addcard("C", 10, "images/17.png");
                this.addcard("C", 11, "images/13.png");
                this.addcard("C", 12, "images/9.png");
                this.addcard("C", 13, "images/5.png");
                this.addcard("C", 14, "images/1.png");
                rand = new Random();
            }
            public void addcard(string suit, int value, string name)
            {
                card newcard = new card(suit, value, name);
                this.Add(newcard);
            }
            public card dealcard()
            {
                int n = rand.Next(0,this.Count());
                card selected = this[n];
                this.RemoveAt(n);
                return selected;
            }
            public Random rand;
            
        }

        public class hand: List<card>
        {
            public hand() : base() { }
            public void organize()
            {
                List<card> spades = this.FindAll(
                    delegate(card cd)
                    {
                        return cd.cardsuit == "S";
                    }
                    );
                List<card> diamonds = this.FindAll(
                    delegate(card cd)
                    {
                        return cd.cardsuit == "D";
                    }
                    );
                List<card> clubs = this.FindAll(
                    delegate(card cd)
                    {
                        return cd.cardsuit == "C";
                    }
                    );
                List<card> hearts = this.FindAll(
                    delegate(card cd)
                    {
                        return cd.cardsuit == "H";
                    }
                    );
                this.Clear();
                this.AddRange(spades);
                this.AddRange(diamonds);
                this.AddRange(clubs);
                this.AddRange(hearts);
            }
        }

        public class players : List<oneplayer>
        {
            public players(int n)
            {
                for (int i = 1; i <= n; i++)
                {
                    oneplayer newplayer = new oneplayer(i);
                    this.Add(newplayer);
                }
            }
        }

        public class card
        {
            public card(string suit, int value, string name)
            {
                cardvalue = value;
                cardsuit = suit;
                cardname = name;
            }
            public string CardToString()
            {
                string valuename;
                string suitname;
                if(this.cardvalue <= 10)
                {
                    valuename = this.cardvalue.ToString();
                } else if(this.cardvalue == 11)
                {
                    valuename = "Jack";
                } else if(this.cardvalue == 12)
                {
                    valuename = "Queen";
                } else if(this.cardvalue == 13)
                {
                    valuename = "King";
                }
                else
                {
                    valuename = "Ace";
                }
                if(this.cardsuit == "C")
                {
                    suitname = "Clubs";
                } else if(this.cardsuit == "S")
                {
                    suitname = "Spades";
                } else if(this.cardsuit == "D")
                {
                    suitname = "Diamonds";
                }
                else
                {
                    suitname = "Hearts";
                }
                return valuename + " of " + suitname;
            }
            public int cardvalue { get; set; }
            public int cardholder { get; set; }
            public string cardsuit { get; set; }
            public string cardname { get; set; }
        }

        public class trick: List<card>
        {
            public trick() : base() {  }
            public card starter { get; set; }
            public card winningcard;
            public Random rand;
            public void playthiscard(oneplayer p, card c)
            {
                if (this.Count()==0)
                {
                    starter = c;
                }
                this.Add(c);
                p.playerhand.Remove(c);
            }
            public void starttrick(players plrs, card trumpcard, int j)
            {
                for(int i=j; i<plrs.Count(); i++)
                {
                    this.playcard(plrs[i], trumpcard);
                }
            }
            public void finishtrick(players plrs, card trumpcard)
            {
                for(int i=1; i<plrs.Count(); i++)
                {
                    if(plrs[i].playerhand.Count() > plrs[0].playerhand.Count())
                    {
                        this.playcard(plrs[i], trumpcard);
                    }
                }
            }
            public void playcard(oneplayer p, card trumpcard)
            { 
                if(this.Count==0)  // play random card to start trick
                {
                    rand = new Random();
                    int n = rand.Next(0, p.playerhand.Count());
                    this.starter = p.playerhand[n];
                    this.Add(starter);
                    p.playerhand.RemoveAt(n);
                }
                else
                {
                    // play following suit, or trump, or random card
                    rand = new Random();
                    List<card> suitcards = p.playerhand.FindAll(
                        delegate(card cd)
                        {
                            return cd.cardsuit == starter.cardsuit;
                        }
                        );
                    if(suitcards.Count()!=0)
                    {
                        int n = rand.Next(0, suitcards.Count());
                        this.Add(suitcards[n]);
                        p.playerhand.Remove(suitcards[n]);
                    }
                    else
                    {
                        List<card> trumpcards = p.playerhand.FindAll(
                        delegate(card cd)
                        {
                            return cd.cardsuit == trumpcard.cardsuit;
                        }
                        );
                        if(trumpcards.Count()!=0)
                        {
                            int n = rand.Next(0, trumpcards.Count());
                            this.Add(trumpcards[n]);
                            p.playerhand.Remove(trumpcards[n]);
                        }
                        else
                        {
                            int n = rand.Next(0, p.playerhand.Count());
                            this.Add(p.playerhand[n]);
                            p.playerhand.RemoveAt(n);
                        }

                    }
                }
            }
            public void identifywinningcard(card trumpcard)
            {
                List<card> trumpcards = this.FindAll(
                    delegate(card cd)
                    {
                        return cd.cardsuit == trumpcard.cardsuit;
                    }
                    );
                List<card> suitcards = this.FindAll(
                    delegate(card cd)
                    {
                        return cd.cardsuit == starter.cardsuit;
                    }
                    );
                if (trumpcards.Count() != 0)
                {
                    List<int> trumpvalues = new List<int>();
                    for (int i = 0; i < trumpcards.Count(); i++ )
                    {
                        trumpvalues.Add(trumpcards[i].cardvalue);
                    }
                    this.winningcard = trumpcards.Find(
                        delegate(card cd)
                        {
                            return cd.cardvalue == trumpvalues.Max();
                        }
                        );
                } else {
                    List<int> leadvalues = new List<int>();
                    for (int i=0; i < suitcards.Count(); i++)
                    {
                        leadvalues.Add(suitcards[i].cardvalue);
                    }
                    this.winningcard = suitcards.Find(
                        delegate(card cd)
                        {
                            return cd.cardvalue == leadvalues.Max();
                        }
                        );
                }
            }
        }


        protected void Button_Play_Click(object sender, EventArgs e)
        {          
            players allplayers = new players(int.Parse(NumPlayers.Text));
            currentgame = new game(allplayers);
            PlaceHolder_Round.Visible = true;
            PlaceHolder_Bid.Visible = false;
            Label_Result.Text = "";
            Winner.Text = "";
            PlaceHolder_Tricks.Controls.Clear();
            Repeater_MyCards.Controls.Clear();
            TrumpSuit.Visible = false;
            scorecard.Clear();
            scorecard.Columns.Clear();
            Scores.Controls.Clear();
            PlaceHolder_Tricks.Controls.Clear();
            for (int i = 1; i <= int.Parse(NumPlayers.Text); i++)
            {
                DataColumn col = new DataColumn();
                col.ColumnName = "Player " + i;
                col.DataType = System.Type.GetType("System.Int32");
                scorecard.Columns.Add(col);
            }
            Session["scores"] = scorecard;
            Session["savedgame"] = currentgame;
        }

        protected void Button_Round_Click(object sender, EventArgs e)
        {
            currentgame = (game)Session["savedgame"];
            if (int.Parse(NumCards.Text)*currentgame.numplayers > 52)
            {
                MsgBox_NoBid.alert("Not enough cards to deal " + NumCards.Text + " to each player.");
            } else
            {
                round currentround = new round(currentgame, int.Parse(NumCards.Text));
                currentgame.Add(currentround);
                Session["savedgame"] = currentgame;
                TrumpSuit.Visible = true;
                TrumpSuit.ImageUrl = currentround.roundtrump.cardname;
                Repeater_MyCards.DataSource = currentgame.gameplayers[0].playerhand;
                Repeater_MyCards.DataBind();
                PlaceHolder_Bid.Visible = true;
                PlaceHolder_Round.Visible = false;
                PlaceHolder_Tricks.Controls.Clear();
                Label_Result.Controls.Clear();
                Winner.Controls.Clear();
            }
        }
        protected void Button_Bid_Click(object sender, EventArgs e)
        {
            currentgame = (game)Session["savedgame"];
            // user enters his bid
            currentgame.gameplayers[0].numwanted = int.Parse(TextBox_Bid.Text);
            PlaceHolder_Bid.Visible = false;
            // opponents bid 1
            for (int i = 1; i < currentgame.numplayers; i++ )
            {
                currentgame.gameplayers[i].numwanted = 1;
            }

            currentgame.Last().biddingstarted = true;
            PlaceHolder_Tricks.Controls.Clear();
            Winner.Text = "";
            // Add a new list of tricks to the round
            currentgame.Last().tricks = new List<trick>();

            // Begin the first trick of the round, starting with Player 2
            trick firsttrick = new trick();
            firsttrick.starttrick(currentgame.gameplayers, currentgame.Last().roundtrump, 1);
            currentgame.Last().tricks.Add(firsttrick);
            
            // show played cards to user
            showtrick = new Table();
            TableRow trow = new TableRow();
            showtrick.Rows.Add(trow);
            showtrick.Controls.Add(trow);
            for (int i = 0; i < currentgame.gameplayers.Count(); i++)
            {
                TableCell tcell = new TableCell();
                trow.Controls.Add(tcell);
            }
            for(int i = 0; i < currentgame.gameplayers.Count()-1; i++)
            {
                Image newimage = new Image();
                newimage.ImageUrl = currentgame.Last().tricks.Last()[i].cardname;
                trow.Cells[currentgame.Last().tricks.Last()[i].cardholder-1].Controls.Add(newimage);
            }
            PlaceHolder_Tricks.Controls.Add(showtrick);
            Label_Result.Text = "You want to win " + currentgame.gameplayers[0].numwanted.ToString() + " tricks.";
            Session["showntrick"] = showtrick;
            Session["savedgame"] = currentgame;
        }
        protected void Repeater_MyCards_ItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            currentgame = (game)Session["savedgame"];
            scorecard = (DataTable)Session["scores"];
            if (currentgame.Last().biddingstarted == false)
            {
                MsgBox_NoBid.alert("You must enter a bid first.");
            }
            else
            {
                // get user card
                card usercard = currentgame.gameplayers[0].playerhand.Find(
                    delegate(card cd)
                    {
                        return cd.cardname == ((ImageButton)e.CommandSource).ImageUrl;
                    }
                    );
                // see if the user followed suit
                bool followedsuit = true;
                // if the user is not starting the trick
                if (currentgame.Last().tricks.Last().Count() != currentgame.gameplayers.Count())
                {
                    // if the user did not follow suit
                    if (usercard.cardsuit != currentgame.Last().tricks.Last().starter.cardsuit)
                    {
                        // check to see if they have any of that suit
                        List<card> suitedcards = currentgame.gameplayers[0].playerhand.FindAll(
                        delegate(card cd)
                        {
                            return cd.cardsuit == currentgame.Last().tricks.Last().starter.cardsuit;
                        }
                        );
                        // if they do, they played a bad card!
                        if (suitedcards.Count() > 0)
                        {
                            followedsuit = false;
                        }
                    }
                }
                if (!followedsuit)
                {
                    MsgBox_NoBid.alert("You have to follow suit!");
                }
                else
                {
                    // see if the user has made a bid already
                    // see if the user is the first card in the trick
                    if (currentgame.Last().tricks.Last().Count() == currentgame.gameplayers.Count())
                    {
                        // if it is then begin a new trick
                        trick nexttrick = new trick();
                        nexttrick.playthiscard(currentgame.gameplayers[0], usercard);
                        currentgame.Last().tricks.Add(nexttrick);
                    }
                    else
                    {
                        // otherwise play the user card in the ongoing trick
                        currentgame.Last().tricks.Last().playthiscard(currentgame.gameplayers[0], usercard);
                    }

                    // finish off the trick
                    currentgame.Last().tricks.Last().finishtrick(currentgame.gameplayers, currentgame.Last().roundtrump);

                    // find the winning card
                    currentgame.Last().tricks.Last().identifywinningcard(currentgame.Last().roundtrump);
                    card winningcard = currentgame.Last().tricks.Last().winningcard;

                    // give the player their trick
                    currentgame.gameplayers[winningcard.cardholder - 1].numwon++;
                    Winner.Text = "Player " + winningcard.cardholder.ToString() + " wins with the " + winningcard.CardToString() + ".";

                    // tell player how many tricks they've won
                    Label_Result.Text = "You've won " + currentgame.gameplayers[0].numwon.ToString() + " out of " + currentgame.gameplayers[0].numwanted.ToString() + " tricks.";
                    // update the user's hand
                    Repeater_MyCards.Controls.Clear();
                    Repeater_MyCards.DataSource = currentgame.gameplayers[0].playerhand;
                    Repeater_MyCards.DataBind();

                    // update the cards played
                    showtrick = new Table();
                    TableRow trow = new TableRow();
                    showtrick.Rows.Add(trow);
                    showtrick.Controls.Add(trow);
                    for (int i = 0; i < currentgame.gameplayers.Count(); i++)
                    {
                        TableCell tcell = new TableCell();
                        trow.Controls.Add(tcell);
                    }
                    for (int i = 0; i < currentgame.gameplayers.Count(); i++)
                    {
                        Image newimage = new Image();
                        newimage.ImageUrl = currentgame.Last().tricks.Last()[i].cardname;
                        trow.Cells[currentgame.Last().tricks.Last()[i].cardholder - 1].Controls.Add(newimage);
                    }
                    PlaceHolder_Tricks.Controls.Clear();
                    PlaceHolder_Tricks.Controls.Add(showtrick);
                    Session["showntrick"] = showtrick;

                    // check to see if there are more tricks to be played, and begin the next trick
                    if (currentgame.gameplayers[0].playerhand.Count() != 0)
                    {
                        if (winningcard.cardholder != 1)
                        {
                            trick newtrick = new trick();
                            newtrick.starttrick(currentgame.gameplayers, currentgame.Last().roundtrump, winningcard.cardholder - 1);
                            currentgame.Last().tricks.Add(newtrick);
                            Table newtable = new Table();
                            TableRow newrow = new TableRow();
                            showtrick.Rows.Add(newrow);
                            showtrick.Controls.Add(newrow);
                            for (int i = 0; i < currentgame.gameplayers.Count(); i++)
                            {
                                TableCell newcell = new TableCell();
                                newrow.Controls.Add(newcell);
                            }
                            for (int i = winningcard.cardholder - 1; i < currentgame.gameplayers.Count(); i++)
                            {
                                Image newimage = new Image();
                                newimage.ImageUrl = currentgame.Last().tricks.Last()[i - winningcard.cardholder + 1].cardname;
                                newrow.Cells[i].Controls.Add(newimage);
                            }
                            PlaceHolder_Tricks.Controls.Add(showtrick);
                            Session["showntrick"] = showtrick;
                        }
                    }
                    else
                    {
                        // say whether player won or lost
                        if (currentgame.gameplayers[0].numwanted == currentgame.gameplayers[0].numwon)
                        {
                            Label_Result.Text = "Congratulations!  You bid " + currentgame.gameplayers[0].numwanted.ToString() + " and you won " + currentgame.gameplayers[0].numwon.ToString() + ".";
                        }
                        else
                        {
                            Label_Result.Text = "You bid " + currentgame.gameplayers[0].numwanted.ToString() + " and you won " + currentgame.gameplayers[0].numwon.ToString() + ".  Too bad!";
                        }
                        // update player scores
                        foreach (oneplayer p in currentgame.gameplayers)
                        {
                            if (p.numwanted == p.numwon)
                            {
                                p.score = p.score + 10 + p.numwon;
                            }
                        }
                        // View PlaceHolder to start next round
                        PlaceHolder_Round.Visible = true;

                        // Update the scorecard
                        DataRow newrow = scorecard.NewRow();
                        for (int i = 0; i < currentgame.gameplayers.Count(); i++)
                        {
                            newrow[i] = currentgame.gameplayers[i].score;
                        }
                        scorecard.Rows.Add(newrow);
                        Scores.DataSource = scorecard;
                        Scores.DataBind();
                        Session["scores"] = scorecard;
                        PlaceHolder_Bid.Visible = false;
                    }
                    Session["savedgame"] = currentgame;
                }
            }
        }
    }
}