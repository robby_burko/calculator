﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Calculator2
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        
        }
        protected void Button_Calculate_Click(object sender, EventArgs e)
        {
            StringToFormula stf = new StringToFormula();
            AnswerBox.Text = stf.Eval(FormulaBox.Text).ToString();
        }
       
    
    }
}