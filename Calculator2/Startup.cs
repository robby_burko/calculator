﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Calculator2.Startup))]
namespace Calculator2
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
