﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace Calculator2
{
    public partial class TableMaker : System.Web.UI.Page
    {
        public Table EntryTable = new Table();
        public DataTable CurrentTable = new DataTable();
        public string CurrentName;
        public int rows;
        public int columns;
        public TableList SavedTables = new TableList();

        protected void Page_Load(object sender, System.EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                EntryTable.ID = "TheTable";
            }
            else
            {
                EntryTable = (Table)Session["table"];
                SavedTables = (TableList)Session["all"];
                this.Controls.Add(EntryTable);
                CurrentTable.Clear();
                int rnum = EntryTable.Rows.Count;
                int cnum;
                if (!EntryTable.Rows.Count.Equals(0))
                {
                    cnum = EntryTable.Rows[0].Cells.Count;
                }
                else
                {
                    cnum = 0;
                }
                for (int i = 0; i < cnum; i++)
                {
                    DataColumn col = new DataColumn();
                    col.DataType = System.Type.GetType("System.String");
                    CurrentTable.Columns.Add(col);
                }

                for (int i = 1; i <= rnum; i++)
                {
                    DataRow row = CurrentTable.NewRow();
                    for (int j = 1; j <= cnum; j++)
                    {
                        row[j-1] = Request.Form["ctl00$MainContent$TB" + i.ToString() + j.ToString()];
                    }
                    CurrentTable.Rows.Add(row);
                }
                CurrentName = TextBox_Name.Text;

                    for (int rcnt = 1; rcnt <= rnum; rcnt++)
                    {
                        for (int ccnt = 1; ccnt <= cnum; ccnt++)
                        {
                            TextBox TB = (TextBox)EntryTable.Rows[rcnt-1].Cells[ccnt-1].FindControl("TB" + rcnt.ToString() + ccnt.ToString());
                             if (!string.IsNullOrEmpty(Request.Form["ctl00$MainContent$TB" + rcnt.ToString() + ccnt.ToString()]))
                            {
                                TB.Text = Request.Form["ctl00$MainContent$TB" + rcnt.ToString() + ccnt.ToString()];
                            }
                        }
                    }
                    if (Request.Form["save_conf"] == "1")   //if user clicks "OK" to confirm 
                    {
                        Request.Form["save_conf"].Replace("1", "0");
                        //Reset the hidden field back to original value "0"
                        OneTable CT = new OneTable(CurrentTable, CurrentName);
                        SavedTables.Add(CT);
                        Session["all"] = SavedTables;
                        Repeater1.DataSource = SavedTables;
                        Repeater1.DataBind();
                        Panel3.Visible = true;
                    } 
            }
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            Session["table"] = EntryTable;
            Session["all"] = SavedTables;
            PlaceHolder.Controls.Add(EntryTable);
        }
        protected void Button_Create_Table_Click(object sender, System.EventArgs e)
        {
            Panel2.Visible = true;
            EntryTable.Controls.Clear();
            int rcnt;
            int ccnt;
            try
            {
                rows = int.Parse(NumRows.Text);
                columns = int.Parse(NumCols.Text);
                rows = int.Parse(NumRows.Text);
                columns = int.Parse(NumCols.Text);
                for (rcnt = 1; rcnt <= rows; rcnt++)
                {
                    TableRow trow = new TableRow();
                    trow.ID = "TR" + rcnt.ToString();
                    EntryTable.Rows.Add(trow);
                    EntryTable.Controls.Add(trow);
                    for (ccnt = 1; ccnt <= columns; ccnt++)
                    {
                        TableCell tcell = new TableCell();
                        tcell.ID = "TC" + rcnt.ToString() + ccnt.ToString();
                        TextBox TB = new TextBox();
                        TB.ID = "TB" + rcnt.ToString() + ccnt.ToString();
                        tcell.Controls.Add(TB);
                        trow.Cells.Add(tcell);
                        trow.Controls.Add(tcell);
                    }
                }
                PlaceHolder.Controls.Add(EntryTable);
                Session["table"] = EntryTable;
            }
            catch 
            { 
            }
            
        }

        protected void Button_Save_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(TextBox_Name.Text))
            { 
                MsgBox1.confirm("You have not entered your name.  Are you sure you want to continue?", "save_conf");
            } else
            { 
            OneTable CT = new OneTable(CurrentTable, CurrentName);
            SavedTables.Add(CT);
            Session["all"] = SavedTables;
            Repeater1.DataSource = SavedTables;
            Repeater1.DataBind();
            Panel3.Visible = true;
            }
        }

      protected void Button_GoHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }
        protected void Button_Show_Click(object sender, EventArgs e)
      {
          Repeater1.Visible = true;
      }
        protected void Button_Hide_Click(object sener, EventArgs e)
        {
            Repeater1.Visible = false;
        }
        protected void Button_Clear_Click(object sener, EventArgs e)
        {
            Repeater1.Controls.Clear();
        }
        public class TableList:List<OneTable>
        {
            public TableList() : base() { }
            public void AddTable(DataTable newDT, string newname)
            {
                OneTable newItem = new OneTable(newDT, newname);
                this.Add(newItem);
            }

        }
        public class OneTable
        {
            public OneTable(DataTable thistable, string name)
            {
                mytable = thistable;
                myname = name;
            }

            public string myname { get; set; }
            public DataTable mytable { get; set; }
        }

    }
}