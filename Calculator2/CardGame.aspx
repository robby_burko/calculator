﻿<%@ Register TagPrefix="cc1" Namespace="BunnyBear" Assembly="msgBox" %>
<%@ Page Title ="Bugger Bridge" Language="C#" AutoEventWireup="true" CodeBehind="CardGame.aspx.cs" MasterPageFile="~/Site.Master" Inherits="Calculator2.CardGame" %>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    <div><h2>Bugger Bridge</h2></div>
    <div>
        <asp:Panel runat="server" DefaultButton="Button_Play">
        <asp:PlaceHolder ID="PlaceHolder_Game" runat="server">
            <p>
                How many players are there?
                <asp:TextBox ID="NumPlayers" runat="server"></asp:TextBox>
                <asp:Button ID="Button_Play" OnClick="Button_Play_Click" runat="server" Text="Begin New Game" /> 
            </p>
        </asp:PlaceHolder>
         </asp:Panel>
        <asp:Panel runat="server" DefaultButton="Button_Round">
            <asp:PlaceHolder ID="PlaceHolder_Round" runat = "server" Visible="false">
                <p>
                    How many cards would you like dealt this round?
                    <asp:TextBox ID="NumCards" runat="server"></asp:TextBox>
                    <asp:Button ID="Button_Round" OnClick="Button_Round_Click" runat="server" Text="Deal" />
                </p>
            </asp:PlaceHolder>
         </asp:Panel>
        <asp:Panel runat="server" DefaultButton="Button_Bid">
        <asp:PlaceHolder ID="PlaceHolder_Bid" runat ="server" Visible ="false">
        <p>
            What would you like to bid? 
            <asp:TextBox ID="TextBox_Bid" runat="server">
            </asp:TextBox>
            <asp:Button ID="Button_Bid" runat="server" Text="Bid" OnClick="Button_Bid_Click" />
        </p>
        </asp:PlaceHolder>
         </asp:Panel>
        <div>
            <asp:Label ID ="Label_Result" runat="server" Font-Size="Large"></asp:Label>
        </div>
        <p>
             <asp:Image ID ="TrumpSuit" runat ="server"></asp:Image>
        </p>
        <p>
        <asp:Repeater ID ="Repeater_MyCards" OnItemCommand = "Repeater_MyCards_ItemCommand" runat ="server" EnableViewState="true">
        <ItemTemplate>
             <asp:ImageButton ID ="Button_MyCards" EnableViewState="true" runat ="server" ImageUrl ='<%#Eval("cardname")%>'></asp:ImageButton>
            </ItemTemplate>
         </asp:Repeater>
         </p>
    </div>
    <div>
        <asp:Label ID="Winner" runat ="server"></asp:Label>
    </div>
    <div>
        <asp:PlaceHolder ID ="PlaceHolder_Tricks" runat="server" EnableViewState="true"></asp:PlaceHolder>
    </div>
    <div>
        <p>
            <asp:GridView ID="Scores" runat="server" Width ="50%" EnableViewState="true"></asp:GridView>
        </p>
    </div>
    <cc1:msgBox ID="MsgBox_NoBid" runat="server"></cc1:msgBox>
</asp:Content>
