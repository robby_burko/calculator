﻿<%@ Register TagPrefix="cc1" Namespace="BunnyBear" Assembly="msgBox" %>
<%@ Page Title="TableMaker" Language ="C#" AutoEventWireup="true" EnableEventValidation="false" MasterPageFile="~/Site.Master" CodeBehind="TableMaker.aspx.cs" Inherits="Calculator2.TableMaker" EnableViewState=true %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <p>How big would you like your table to be?</p>
    <asp:Panel ID="TableMakerPanel" runat="server" DefaultButton="Button_Create_Table">
        <p>
            Number of Rows:
        <asp:TextBox ID="NumRows" runat="server"></asp:TextBox>
            Number of Columns:
        <asp:TextBox ID="NumCols" runat="server"></asp:TextBox>
            <asp:Button ID="Button_Create_Table" OnClick="Button_Create_Table_Click" runat="server" Text="Create Table" />
        </p>
    </asp:Panel>
    <asp:PlaceHolder ID="PlaceHolder" runat="server"></asp:PlaceHolder>

      <asp:Panel ID="Panel2" runat="server" DefaultButton="Button_Save" Visible="false">
        <p>
            Type your name:
            <asp:TextBox ID="TextBox_Name" runat="server"></asp:TextBox>
            <asp:Button ID="Button_Save" OnClick="Button_Save_Click" runat="server" Text="Save" />
        </p>
    </asp:Panel>
     <asp:Repeater ID ="Repeater1" runat ="server" EnableViewState="true" Visible ="false">
        <ItemTemplate>
         <asp:Label ID ="Label_Name" runat ="server" Text ='<%#Eval("myname")%>'></asp:Label>
        <asp:GridView ID="RepeaterTable" runat="server" ShowHeader="false" Width ="50%" DataSource ='<%#Eval("mytable")%>'> </asp:GridView>
          </ItemTemplate>
    </asp:Repeater>
    <asp:Panel ID="Panel3" runat ="server" Visible ="false">
        <p>
        <asp:Button ID="Button_Show" OnClick="Button_Show_Click" runat="server" Text="Show Saved Tables" />
        <asp:Button ID="Button_Hide" OnClick="Button_Hide_Click" runat="server" Text="Hide Saved Tables" />
        <asp:Button ID="Button_Clear" OnClick="Button_Clear_Click" runat="server" Text="Clear Saved Tables" />
        </p>
    </asp:Panel>
    <asp:Button ID="Button_GoHome" runat="server" Text="Home" OnClick ="Button_GoHome_Click" />
   <cc1:msgBox ID="MsgBox1" runat="server"></cc1:msgBox>
</asp:Content>
