﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Calculator2
{
    public partial class RepeaterExample : System.Web.UI.Page
    {
        SetOfItems setOfItems;

        string[] words;
        int[] numbers;

        protected void Page_Load(object sender, EventArgs e)
        {
            /// what has to be done the first time the page is ran
            if (!Page.IsPostBack)
            {
                /// I have to put the data somewhere !
                words = new string[10];
                numbers = new int[10];
                words[0] = "uno";
                words[1] = "dos";
                words[2] = "tres";
                words[3] = "cuatro";
                words[4] = "cinco";
                words[5] = "seis";
                words[6] = "siete";
                words[7] = "ocho";
                words[8] = "...";
                words[9] = "MAaaaMBOÓooo !! UH !";

                int j = 0;
                foreach (int a in numbers)
                {
                    //a = j;  uncomment, check this error and understand it.
                    j++;
                }

                // This is the way to do it, why?
                for (int i = 0; i < 10; i++) 
                {
                    numbers[i] = i + 1;
                }

                setOfItems = new SetOfItems();
                setOfItems.AddAnItem(numbers[0], words[0]);
                Session.Add("SetOfItmes", setOfItems);
                Session.Add("Numbers", numbers);
                Session.Add("Words", words);


                TestRepeater.DataSource = setOfItems;
                TestRepeater.DataBind();
            }
            else /// what has to be done if this is not the first time the page is ran.
            {


            }
            /// what has to be done, regardless of what
            
            /// Uncheck these part of the code, and change the content of a Text Box.
            /// In breakes down !! why? Welcome the world of events !!
        //    TestRepeater.DataSource = setOfItems;
        //    TestRepeater.DataBind();
        }


        protected void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }

        #region Event Methods
        protected void Add_Click(object sender, EventArgs e)
        {
            setOfItems = (SetOfItems)Session["SetOfItmes"];
            numbers = (int[])Session["Numbers"];
            words = (string[])Session["Words"];
            int last = setOfItems.Count;
            OneItemData newItem;
            if (last < 10)
            {
                newItem = new OneItemData(numbers[last], words[last]);
            }
            else 
            {
                newItem = new OneItemData(last+1, "Here's Another");
            }
            setOfItems.Add(newItem);
            TestRepeater.DataSource = setOfItems;
            TestRepeater.DataBind();

            /// Question: In this function, I bring from Session the setOfItems
            /// then I change its content.
            /// The I use it, but I don't save it back to Session. 
            /// Yet the page works i.e. it remembers the content of setOfItems he next time I click on Add; why?
            /// read about pointers, if you don't know about it yet.
        }

        protected void Minus_Click(object sender, EventArgs e)
        {
            setOfItems = (SetOfItems)Session["SetOfItmes"];
            int last = setOfItems.Count - 1;
            setOfItems.RemoveAt(last);
            TestRepeater.DataSource = setOfItems;
            TestRepeater.DataBind();
        }

        /// <summary>
        /// If one of the text box content is changes, this function is called.
        /// But, which TextBox? 
        /// </summary>
        /// <param name="sender"> This Text Box, the sender !! </param>
        /// <param name="e"></param>
        protected void OnTextChange(object sender, EventArgs e) 
        {
            /// This line tells you how to obtain the ID of the sender of the event.
            /// Why did I casted it as a WebControl ?
            /// if you don't know what is casting, google it: it's important.
            /// now put a break point inside this function and place the mouse on sender.
            /// look for ClientID. Does it makes sence now? 
            /// Wellcome to the world of inheritance !! this is important.
            string s = ((WebControl)sender).ClientID;

            /// Finish the example by saving the data in words and numebrs.


            TestRepeater.DataSource = setOfItems;
            TestRepeater.DataBind();
        }

        #endregion EventMethods
    }




    /// <summary>
    /// This class is just a data container.
    /// </summary>
    public class OneItemData 
    {
        public OneItemData(int numberForColOne, string stringForColTwo)
        {
            colOne = numberForColOne;
            colTwo = stringForColTwo;
        }

        public int colOne { get; set; }
        public string colTwo { get; set; }
    }

    /*
     
     */
    /// <summary>
    /// This is a subclass of a List of OneItemData.
    /// It inherets all the functions of List<>, and you can add your own.
    /// That is, List<T> is a class previously implemented by C#,
    /// SetOfItems is a List<OneItemData> too, PLUS !! whatever I want to add to it.
    /// </summary>
    public class SetOfItems : List<OneItemData>
    { 
        /// <summary>
        /// The constructor has to run base(), which is the constructor of the base class List<T>
        /// </summary>
        public SetOfItems() : base() {}

        /// <summary>
        /// I added this function.
        /// </summary>
        /// <param name="newColOne"></param>
        /// <param name="newColTwo"></param>
        public void AddAnItem(int newColOne, string newColTwo)
        {
            /// which creates a newItem 
            OneItemData newItem = new OneItemData(newColOne, newColTwo);
            
            /// stores it in the list.
            /// this is a pointer the object itself. 
            /// If you don't get it, read some of it online, and then ask me, This is important.
            this.Add(newItem);
        }
    }
} 