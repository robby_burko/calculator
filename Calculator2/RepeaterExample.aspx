﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="True" CodeBehind="RepeaterExample.aspx.cs" Inherits="Calculator2.RepeaterExample" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <h1>Example of a repeater</h1>
    
    <asp:Repeater ID="TestRepeater" runat="server" EnableViewState="true">
        <HeaderTemplate>
            <table>
                <tr>
                    <th>Column one</th> <th>Column two</th>
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
                <tr>
                    <td>
                        <asp:TextBox ID="ColOneTextBox" runat="server" AutoPostBack="true" Text= '<%# Eval("colOne").ToString() %>' OnTextChanged="OnTextChange"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Label ID="ColTwoLabel" runat="server" EnableViewState="true" Text = '<%# Eval("colTwo") %>'></asp:Label>
                    </td>
                </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:Repeater>

    <asp:Button ID="Add" Text="Add" runat="server" OnClick="Add_Click"/>
    <asp:Button ID="Minus" Text="Minus" runat="server" OnClick="Minus_Click" />

</asp:Content>
