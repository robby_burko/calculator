﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Calculator2._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1>Calculator</h1>
    </div>

    <asp:TextBox ID="FormulaBox" runat="server"></asp:TextBox>
    <asp:Button ID="Button_Calculate" runat="server" Text="Calculate!" OnClick ="Button_Calculate_Click" />
    <asp:TextBox ID="AnswerBox" runat="server"></asp:TextBox>

    <div><a runat ="server" href="~/TableMaker">Try Our Table Maker!</a></div>
    <div><a runat="server" href="~/CardGame">Play Bugger Bridge!</a></div>

</asp:Content>
